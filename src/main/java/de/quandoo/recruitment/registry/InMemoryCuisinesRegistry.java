package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.dao.DbAccess;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.LinkedList;
import java.util.List;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private DbAccess db = DbAccess.getInstance();

    /**
     * Method to add preferenced cuisine to customer
     * @author Zaleskovskiy Mikhail
     * @param customer
     * @param cuisine
     */
    @Override
    public void register(Customer customer, Cuisine cuisine) {
        db.addPreference(customer, cuisine);
    }

    /**
     * Return the list of preferenced cuisines for specific customer
     * @author Zaleskovskiy Mikhail
     * @param customer
     * @return List of Cuisines
     */
    @Override
    public List<Cuisine> customerCuisines(Customer customer) {
        return new LinkedList<>(db.getCuisinesForCustomer(customer.getUuid()));
    }

    /**
     * Return list of customers which prefer specific cuisine
     * @author Zaleskovskiy Mikhail
     * @param cuisine
     * @return List of Customers
     */
    @Override
    public List<Customer> cuisineCustomers(Cuisine cuisine) {
        return new LinkedList<>(db.getCustomersForCuisine(cuisine.getName()));
    }

    /**
     * Return list of the most popular cusines not longer the specific length
     * @author Zaleskovskiy Mikhail
     * @param length
     * @return List of Cuisines
     */
    @Override
    public List<Cuisine> topCuisines(int length) {
        return db.getTopCuisinesList(length);
    }
}
