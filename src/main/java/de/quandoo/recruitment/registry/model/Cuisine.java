package de.quandoo.recruitment.registry.model;

public class Cuisine {

    private final String name;

    public Cuisine(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        return name.equalsIgnoreCase(((Cuisine)obj).getName());
    }

    @Override
    public String toString() {
        return name;
    }
}
