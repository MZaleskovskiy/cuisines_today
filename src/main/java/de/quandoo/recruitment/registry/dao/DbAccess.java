package de.quandoo.recruitment.registry.dao;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.stream.Collectors;

public class DbAccess {
    private Map<String, Set<Customer>> customersOfCuisine;
    private Map<String, Set<Cuisine>> cuisinesOfCustomer;

    private static DbAccess instance = new DbAccess();

    private DbAccess() {
        customersOfCuisine = new HashMap<>();
        cuisinesOfCustomer = new HashMap<>();
    }

    public static DbAccess getInstance() {
        return instance;
    }


    public Set<Customer> getCustomersForCuisine(String cuisineName) {
        return customersOfCuisine.get(cuisineName.toLowerCase());
    }

    public Set<Cuisine> getCuisinesForCustomer(final String customerId) {
        return cuisinesOfCustomer.get(customerId);
    }

    public void addPreference(final Customer customer, final Cuisine cuisine){
        Set<Customer> customersOfOneCuisine = customersOfCuisine.get(cuisine.getName().toLowerCase());
        if (customersOfOneCuisine == null){
            customersOfOneCuisine = new HashSet<>();
            customersOfOneCuisine.add(customer);
            customersOfCuisine.put(cuisine.getName().toLowerCase(), customersOfOneCuisine);
        } else {
            customersOfOneCuisine.add(customer);
        }

        Set<Cuisine> cuisinesOfOneCustomer = cuisinesOfCustomer.get(customer.getUuid());
        if (cuisinesOfOneCustomer == null) {
            cuisinesOfOneCustomer = new HashSet<>();
            cuisinesOfOneCustomer.add(cuisine);
            cuisinesOfCustomer.put(customer.getUuid(), cuisinesOfOneCustomer);
        } else {
            cuisinesOfOneCustomer.add(cuisine);
        }
    }

    public List<Cuisine> getTopCuisinesList(final int n){
        return customersOfCuisine.entrySet()
                .stream()
                .sorted(
                    (cus1, cus2) -> cus2.getValue().size() - cus1.getValue().size()
                )
                .limit(n)
                .map(entry -> new Cuisine(entry.getKey()))
                .collect(Collectors.toList());
    }
}
