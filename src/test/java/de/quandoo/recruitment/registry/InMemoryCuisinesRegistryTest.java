package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.*;
import static org.junit.Assert.*;


public class InMemoryCuisinesRegistryTest {

    private static InMemoryCuisinesRegistry reg;
    private static Customer c1;
    private static Customer c2;
    private static Customer c3;
    private static Customer c4;
    private static Cuisine q1;
    private static Cuisine q2;
    private static Cuisine q3;
    private static Cuisine q4;

    @BeforeClass
    public static void setUp(){
        c1 = new Customer("1");
        c2 = new Customer("2");
        c3 = new Customer("3");
        c4 = new Customer("4");
        q1 = new Cuisine("Itallian");
        q2 = new Cuisine("German");
        q3 = new Cuisine("Japanise");
        q4 = new Cuisine("Chinese");
        reg = new InMemoryCuisinesRegistry();
        reg.register(c1, q1);
        reg.register(c2, q2);
        reg.register(c3, q3);
        reg.register(c4, q4);
        reg.register(c4, q3);
        reg.register(c4, q1);
        reg.register(c2, q1);
    }

    @Test
    public void customerCuisines1() {
        Cuisine[] actual = reg.customerCuisines(c1).toArray(new Cuisine[0]);
        Cuisine[] expected = {q1};
        assertArrayEquals(expected, actual);
    }

    @Test
    public void customerCuisines2() {
        List<Cuisine> actual = reg.customerCuisines(c2);
        assertThat(actual, containsInAnyOrder(q2, q1));
    }

    @Test
    public void customerCuisines3() {
        List<Cuisine> actual = reg.customerCuisines(c3);
        assertThat(actual, containsInAnyOrder(q3));
    }

    @Test
    public void customerCuisines4() {
        List<Cuisine> actual = reg.customerCuisines(c4);
        assertThat(actual, containsInAnyOrder(q1, q3, q4));
    }

    @Test
    public void cuisineCustomers1() {
        List<Customer> actual = reg.cuisineCustomers(q1);
        assertThat(actual, containsInAnyOrder(c1, c2, c4));
    }

    @Test
    public void cuisineCustomers2() {
        List<Customer> actual = reg.cuisineCustomers(q2);
        assertThat(actual, containsInAnyOrder(c2));
    }

    @Test
    public void cuisineCustomers3() {
        List<Customer> actual = reg.cuisineCustomers(q3);
        assertThat(actual, containsInAnyOrder(c3, c4));
    }

    @Test
    public void cuisineCustomers4() {
        List<Customer> actual = reg.cuisineCustomers(q4);
        assertThat(actual, containsInAnyOrder(c4));
    }

    @Test
    public void topCuisines() {
        Cuisine[] actual = reg.topCuisines(2).toArray(new Cuisine[0]);
        Cuisine[] expected = {q1, q3};
        assertArrayEquals(expected, actual);

    }
}